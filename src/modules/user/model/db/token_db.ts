import { ObjectId } from 'mongodb';

export interface TokenDB {
  _id: ObjectId;
  accessToken: string;
  refreshToken: string;
  device: string;
  fcmToken: string;
  userId: ObjectId;
}
