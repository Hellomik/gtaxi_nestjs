import { ObjectId } from 'mongodb';

export interface UserDB {
  _id: ObjectId;
  phoneNumber: string;
  email?: string;
  driverId?: ObjectId;
}
