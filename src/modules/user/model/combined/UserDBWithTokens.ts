import { TokenDB } from '../db/token_db';
import { UserDB } from '../db/user_db';

export interface UserDBWithTokens extends UserDB {
  tokens: TokenDB[];
}
