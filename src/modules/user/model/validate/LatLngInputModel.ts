import { IsNumber } from 'class-validator';
import { LatLng } from 'src/proto/gtaxi';


export class LatLngInputModel implements LatLng {
    @IsNumber()
    latitude: number;

    @IsNumber()
    longitude: number;
}
