import { Type } from 'class-transformer';
import { IsNotEmptyObject, ValidateNested } from 'class-validator';
import { ClientPoint, LatLng } from 'src/proto/gtaxi';
import { LatLngInputModel } from './LatLngInputModel';


export class ClientPointInputModel implements ClientPoint {
    @IsNotEmptyObject()
    @ValidateNested()
    @Type(() => LatLngInputModel)
    latLng: LatLng;
}
