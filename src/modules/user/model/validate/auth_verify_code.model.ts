import {
  IsNotEmptyObject,
  IsOptional,
  IsPhoneNumber,
  IsString,
  ValidateNested,
} from 'class-validator';
import { AuthVerifyCodeInput, Device } from 'src/proto/gtaxi';
import { DeviceInputModel } from './device_inp.model';
import { Type } from 'class-transformer';

export class AuthVerifyCodeInputModel implements AuthVerifyCodeInput {
  @IsPhoneNumber()
  phoneNumber: string;
  @IsString()
  code: string;

  @IsString()
  @IsOptional()
  fullName: string;

  @IsNotEmptyObject()
  @ValidateNested()
  @Type(() => DeviceInputModel)
  device: DeviceInputModel;
}
