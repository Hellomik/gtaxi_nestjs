import { IsOptional, IsString } from 'class-validator';
import { Device } from 'src/proto/gtaxi';

export class DeviceInputModel implements Device {
  @IsString()
  deviceId: string;

  @IsString()
  @IsOptional()
  fcmToken?: string;
}
