import { AuthPhoneNumberInput } from 'src/proto/gtaxi';
import { IsPhoneNumber, IsString } from 'class-validator';

export class AuthPhoneNumberInputModel implements AuthPhoneNumberInput {
  @IsString()
  @IsPhoneNumber()
  phoneNumber: string;
}
