import { Type } from 'class-transformer';
import { ArrayMinSize, ValidateNested } from 'class-validator';
import { RequestDriverInput } from 'src/proto/gtaxi';
import { IsNonPrimitiveArray } from 'src/utils/not_primitive_array';
import { ClientPointInputModel } from './ClientPointInputModel';

export class RequestDriverInputModel implements RequestDriverInput {
  @ValidateNested({ each: true })
  @IsNonPrimitiveArray()
  @ArrayMinSize(2)
  @Type(() => ClientPointInputModel)
  points: ClientPointInputModel[];
}
