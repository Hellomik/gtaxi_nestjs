import { Controller } from '@nestjs/common';
import { Observable } from 'rxjs';
import * as proto from 'src/proto/gtaxi';
import { ShieldDecorator } from 'src/utils/shield_decorator';
import { GRPCUser } from '../services/grpc_auth_guard';
import { OrderDB } from 'src/modules/order/model/order_db';
import { ObjectId } from 'mongodb';
import { CreateOrderAsUserService } from 'src/modules/order/services/user/create_order_as_user.service';
import { parsePointOrderDbToPointOrder } from 'src/utils/parse_mongo_geo_to_latlng';
import { Payload } from '@nestjs/microservices';
import { RequestDriverInputModel } from '../model/validate/RequestDriverInput.model';

@Controller()
@proto.UserServiceControllerMethods()
export class UserController implements proto.UserServiceController {
  constructor(private userCreateOrder: CreateOrderAsUserService) {}

  @ShieldDecorator()
  async createOrder(
    @Payload() request: RequestDriverInputModel,
    @GRPCUser() userId?: string,
  ): Promise<proto.Order> {
    const [startPoint, ...routePoints] = request.points;
    const pointOrders: proto.PointOrder[] = [
      {
        point: startPoint.latLng,
        type: proto.PointType.start,
      },
      ...routePoints.map((val) => ({
        point: val.latLng,
        type: proto.PointType.route,
      })),
    ];

    const orderVal = await this.userCreateOrder.createOrder({
      clientPoints: pointOrders,
      userId: new ObjectId(userId),
    });

    return {
      id: orderVal._id.toHexString(),
      pointOrders: orderVal.pointOrders.map((val) =>
        parsePointOrderDbToPointOrder(val),
      ),
      status: orderVal.status,
      driverId: orderVal.driverId?.toHexString(),
      userId: userId,
    };
  }
  userListenOrders(
    request: Observable<proto.EmptyBody>,
  ): Observable<proto.Order> {
    throw new Error('Method not implemented.');
  }
}
