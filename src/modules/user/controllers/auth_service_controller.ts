import {
  Controller,
  Inject,
  UseFilters,
  UsePipes,
  ValidationPipe,
} from '@nestjs/common';
import {
  AuthServiceController,
  AuthServiceControllerMethods,
  SendCodeTelegramResponce,
  UserTokenResponce,
} from 'src/proto/gtaxi';
import { AuthPhoneNumberInputModel } from '../model/validate/send_code_telegram';
import { Http2gRPCExceptionFilter } from 'src/utils/http2gRPCException.filter';
import { SendCodeTelegramService } from 'src/modules/telegram/services/send_code.service.tg';
import { AuthVerifyCodeInputModel } from '../model/validate/auth_verify_code.model';
import { CodeService } from '../services/code.service';

import { JwtService } from '@nestjs/jwt';
import { Collection, Db } from 'mongodb';
import { generateTokensByUserId } from 'src/utils/generate_tokens';
import { UserDB } from '../model/db/user_db';
import { TokenDB } from '../model/db/token_db';
import { cleanPhoneNumber } from 'src/utils/clean_phone_number';
import { IMongoService } from 'src/modules/mongo/mongo.module';

@Controller()
@AuthServiceControllerMethods()
export class AuthController implements AuthServiceController {
  userCollection: Collection<UserDB>;
  tokenCollection: Collection<TokenDB>;

  constructor(
    private sendCodeTelegramService: SendCodeTelegramService,
    private codeService: CodeService,
    private jwtService: JwtService,
    private mongoSevice: IMongoService,
  ) {
    const database = this.mongoSevice.database;
    this.userCollection = database.collection('User');
    this.tokenCollection = database.collection('Token');
  }

  //   MODELED need test
  @UsePipes(new ValidationPipe({ transform: true }))
  @UseFilters(new Http2gRPCExceptionFilter())
  async verifyCode(
    request: AuthVerifyCodeInputModel,
  ): Promise<UserTokenResponce> {
    this.codeService.verifyCode({
      code: request.code,
      phoneNumber: request.phoneNumber,
    });

    const phoneNumber = cleanPhoneNumber(request.phoneNumber);
    const createdUser = await this.userCollection.findOneAndUpdate(
      {
        phoneNumber,
      },
      {
        $set: {
          phoneNumber,
        },
      },
      {
        upsert: true,
        returnDocument: 'after',
      },
    );
    const userId = createdUser!._id;
    const tokens = generateTokensByUserId(
      userId.toHexString(),
      this.jwtService,
    );
    const createdToken = await this.tokenCollection.findOneAndUpdate(
      {
        userId: createdUser!._id,
        device: request.device,
      },
      {
        $set: {
          accessToken: tokens.accessToken,
          refreshToken: tokens.refreshToken,
          device: request.device.deviceId,
          fcmToken: request.device.fcmToken,
          userId: createdUser!._id,
        },
      },
      { upsert: true, returnDocument: 'after' },
    );

    return {
      user: {
        id: createdUser!._id.toHexString(),
        email: createdUser!.email,
        phoneNumber: createdUser!.phoneNumber,
      },
      token: tokens,
    };
  }

  // MODELED need test
  @UsePipes(new ValidationPipe({ transform: true }))
  @UseFilters(new Http2gRPCExceptionFilter())
  async sendCodeTelegram(
    request: AuthPhoneNumberInputModel,
  ): Promise<SendCodeTelegramResponce> {
    console.log(request);
    return {
      codeSent: await this.sendCodeTelegramService.sendCode({
        phoneNumber: request.phoneNumber,
      }),
    };
  }
}
