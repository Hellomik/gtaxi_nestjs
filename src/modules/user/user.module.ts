import { Module, forwardRef } from '@nestjs/common';
import { AuthController } from './controllers/auth_service_controller';
import { CodeService } from './services/code.service';
import { TelegramModule } from '../telegram/telegram.module';
import { UserController } from './controllers/user_service_controller';
import { OrderModule } from '../order/order.module';

@Module({
  imports: [forwardRef(() => TelegramModule), OrderModule],
  providers: [CodeService],
  controllers: [AuthController, UserController],
  exports: [CodeService],
})
export class UserModule {}
