import {
  Injectable,
  CanActivate,
  ExecutionContext,
  createParamDecorator,
} from '@nestjs/common';
import { Observable, throwError } from 'rxjs';
import { JwtService } from '@nestjs/jwt';

@Injectable()
export class GrpcAuthGuard implements CanActivate {
  constructor(private jwtService: JwtService) {}

  getRequest(context: ExecutionContext) {
    return context.switchToRpc().getContext();
  }

  canActivate(
    context: ExecutionContext,
  ): boolean | Promise<boolean> | Observable<boolean> {
    const request = this.getRequest(context);
    const type = context.getType();
    const prefix = 'Bearer ';
    let header;
    if (type === 'rpc') {
      const metadata = context.getArgByIndex(1); // metadata
      if (!metadata) {
        return false;
      }
      header =
        metadata.get('authorization')[0] ?? metadata.get('Authorization')[0];
    }

    if (!header || !header.includes(prefix)) {
      return false;
    }

    const token = header.slice(header.indexOf(' ') + 1);
    try {
      const valid = this.jwtService.verify(token);
      request.userId = valid.id;
      return true;
    } catch (e) {
      return false;
    }
  }
}

export const GRPCUser = createParamDecorator(
  (data: unknown, context: ExecutionContext) => {
    return context.switchToRpc().getContext().userId;
  },
);
