import { Inject, Injectable } from '@nestjs/common';
import { Cron, Interval } from '@nestjs/schedule';
import { Collection, Db } from 'mongodb';
import { DriverDB } from 'src/modules/driver/model/db/driver_db';
import { OrderDB } from 'src/modules/order/model/order_db';
import { IMongoService, mongoClient } from 'src/modules/mongo/mongo.module';
import { DriverStatus, OrderStatus, PointType } from 'src/proto/gtaxi';

@Injectable()
export class OrderFindDriver {
  driverCollection: Collection<DriverDB>;
  orderCollection: Collection<OrderDB>;

  constructor(private mongoService: IMongoService) {
    const database = this.mongoService.database;
    this.orderCollection = database.collection('Order');
    this.driverCollection = database.collection('Driver');
  }

  @Interval('driver_order', 10 * 1000)
  async findDriverHandler() {
    return;
    // const session = mongoClient.startSession();
    // // Step 2: Optional. Define options to use for the transaction

    // try {
    //   await session.withTransaction(
    //     async () => {
    //       const freeDriversCur = this.driverCollection.find({
    //         status: DriverStatus.active,
    //       });
    //       const orderCur = this.orderCollection.find(
    //         {
    //           status: OrderStatus.searching,
    //         },
    //         {
    //           session,
    //         },
    //       );
    //       const [freeDrivers, orders] = await Promise.all([
    //         freeDriversCur.toArray(),
    //         orderCur.toArray(),
    //       ]);
    //       [freeDriversCur.close(), orderCur.close()];
    //       // {
    //       //   sources: [
    //       //     { lat: 43.249961, lon: 76.933463 },
    //       //     { lat: 43.250332, lon: 76.888383 },
    //       //     { lat: 43.232505, lon: 76.869943 },
    //       //     { lat: 43.240365, lon: 76.814794 },
    //       //     { lat: 43.222368, lon: 76.883941 },
    //       //     { lat: 43.217354, lon: 76.902414 },
    //       //     { lat: 43.248654, lon: 76.955995 },
    //       //     { lat: 43.195448, lon: 76.896592 },
    //       //     { lat: 43.143007, lon: 76.901125 },
    //       //   ],
    //       //   targets: [
    //       //     { lat: 43.162367, lon: 77.051706 },
    //       //     { lat: 43.195448, lon: 76.896592 },
    //       //   ],
    //       //   costing: 'auto',
    //       // }
    //       //   {  "sources": [    { "lat": 43.249961, "lon": 76.933463 },    { "lat": 43.250332, "lon": 76.888383 },    { "lat": 43.232505, "lon": 76.869943 },    { "lat": 43.240365, "lon": 76.814794 },    { "lat": 43.222368, "lon": 76.883941 },    { "lat": 43.217354, "lon": 76.902414 },    { "lat": 43.248654, "lon": 76.955995 },    { "lat": 43.195448, "lon": 76.896592 },    { "lat": 43.143007, "lon": 76.901125 }  ],  "targets": [{ lat: 43.162367, lon: 77.051706 }, { "lat": 43.162367, "lon": 77.051706 }],  "costing": "auto"}
    //       const sourcePoints = freeDrivers.map((val) => ({
    //         lon: val.position.coordinates[0],
    //         lat: val.position.coordinates[1],
    //       }));
    //       const targetPoints = orders.map((val) => {
    //         const point = val.pointOrders.find(
    //           (val) => val.type == PointType.start,
    //         ).point;
    //         return {
    //           lon: point.coordinates[0],
    //           lat: point.coordinates[1],
    //         };
    //       });

    //       const bodyApi = {
    //         sources: sourcePoints,
    //         targets: targetPoints,
    //         costing: 'auto',
    //       };
    //     },
    //     {
    //       readPreference: 'primary',
    //       readConcern: 'local',
    //       writeConcern: {
    //         w: 'majority',
    //       },
    //     },
    //   );
    // } finally {
    //   session.endSession();
    // }
  }
}
