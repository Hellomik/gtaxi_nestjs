import { Inject, Injectable } from '@nestjs/common';
import { Collection, Db, ObjectId } from 'mongodb';
import { DriverDB } from 'src/modules/driver/model/db/driver_db';
import { OrderDB } from 'src/modules/order/model/order_db';
import { UserDBWithTokens } from 'src/modules/user/model/combined/UserDBWithTokens';
import { UserDB } from 'src/modules/user/model/db/user_db';
import * as proto from 'src/proto/gtaxi';
import { OrderController } from '../controllers/order_controller';
import { Subject } from 'rxjs';
import { IMongoService } from 'src/modules/mongo/mongo.module';
import { parsePointOrderDbToPointOrder } from 'src/utils/parse_mongo_geo_to_latlng';

@Injectable()
export class DriverOrderService {
  driverCollection: Collection<DriverDB>;
  orderCollection: Collection<OrderDB>;
  userCollection: Collection<UserDB>;
  listenDriverMap: Map<string, Subject<proto.OrdersResponce>[]> = new Map<
    string,
    Subject<proto.OrdersResponce>[]
  >();

  constructor(private mongoService: IMongoService) {
    const database = this.mongoService.database;
    this.orderCollection = database.collection('Order');
    this.driverCollection = database.collection('Driver');
    this.userCollection = database.collection('User');
  }

  async getOrders(userId: ObjectId): Promise<OrderDB[]> {
    const curOrders = this.driverCollection.aggregate<
      DriverDB & { orders: OrderDB[] }
    >([
      {
        $match: {
          userId: new ObjectId(userId),
        },
      },
      {
        $lookup: {
          from: 'Order',
          localField: '_id',
          foreignField: 'driverId',
          as: 'orders',
          pipeline: [
            {
              $match: {
                status: {
                  $in: [
                    proto.OrderStatus.searching,
                    proto.OrderStatus.inProgress,
                    proto.OrderStatus.waitingResponce,
                  ],
                },
              },
            },
          ],
        },
      },
    ]);
    const result = await curOrders.next();
    curOrders.close();

    return result!.orders;
  }
  async notifyDriver(order: OrderDB) {
    const driver = await this.driverCollection.findOne({
      _id: order.driverId,
    });

    if (driver == null) {
      return;
    }

    this.getOrders(driver.userId)
      .then((driverOrders) => {
        const allSubects = this.listenDriverMap.get(order.userId.toHexString());
        allSubects?.forEach((singlSubj) => {
          singlSubj.next({
            orders: driverOrders.map(
              (val): proto.Order => ({
                driverId: val.driverId?.toHexString(),
                id: val._id.toHexString(),
                status: val.status,
                userId: val.userId.toHexString(),
                pointOrders: val.pointOrders.map((val) =>
                  parsePointOrderDbToPointOrder(val),
                ),
              }),
            ),
          });
        });
      })
      .catch((e) => {
        console.log(e);
      });

    try {
      const tokensCur = this.userCollection.aggregate<UserDBWithTokens>([
        {
          $match: {
            driverId: order.driverId,
          },
        },
        {
          $lookup: {
            from: 'Token',
            localField: '_id',
            foreignField: 'userId',
            as: 'tokens',
          },
        },
      ]);
      const tokens = (await tokensCur.next())?.tokens.filter(
        (val) => val.fcmToken != null,
      );
      console.log(tokens);

      tokensCur.close();
    } catch (e) {
      console.log(e);
    }
  }

  async notifyDriverAboutAllOrders(driverId: ObjectId): Promise<void> {
    const driver = await this.driverCollection.findOne({
      _id: driverId,
    });
    if (driver == null) throw 'Driver not found';
    this.getOrders(driver.userId)
      .then((driverOrders) => {
        const allSubects = this.listenDriverMap.get(driver.userId.toHexString());
        allSubects?.forEach((singlSubj) => {
          singlSubj.next({
            orders: driverOrders.map(
              (val): proto.Order => ({
                driverId: val.driverId?.toHexString(),
                id: val._id.toHexString(),
                status: val.status,
                userId: val.userId.toHexString(),
                pointOrders: val.pointOrders.map((val) =>
                  parsePointOrderDbToPointOrder(val),
                ),
              }),
            ),
          });
        });
      })
      .catch((e) => {
        console.log(e);
      });
  }
}
