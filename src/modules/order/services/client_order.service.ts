import { Inject, Injectable } from '@nestjs/common';
import { Collection, Db, ObjectId } from 'mongodb';

import { OrderDB } from 'src/modules/order/model/order_db';
import { OrderStatus, OrdersResponce } from 'src/proto/gtaxi';
import { OrderWithDriverDb } from '../model/order_with_driver_db';
import { DriverDB } from 'src/modules/driver/model/db/driver_db';
import { Subject } from 'rxjs';
import { UserDB } from 'src/modules/user/model/db/user_db';
import { UserDBWithTokens } from 'src/modules/user/model/combined/UserDBWithTokens';
import { IMongoService } from 'src/modules/mongo/mongo.module';
import * as proto from 'src/proto/gtaxi';
import {
  parsePointOrderDbToLantLng,
  parsePointOrderDbToPointOrder,
} from 'src/utils/parse_mongo_geo_to_latlng';

@Injectable()
export class ClientOrderService {
  driverCollection: Collection<DriverDB>;
  orderCollection: Collection<OrderDB>;
  userCollection: Collection<UserDB>;
  listenUserMap: Map<string, Subject<OrdersResponce>[]> = new Map();

  constructor(private mongoService: IMongoService) {
    const database = this.mongoService.database;
    this.orderCollection = database.collection('Order');
    this.driverCollection = database.collection('Driver');
    this.userCollection = database.collection('User');
  }

  async getOrders(userId: ObjectId): Promise<OrderWithDriverDb[]> {
    const curOrders = this.orderCollection.aggregate<OrderWithDriverDb>([
      {
        $match: {
          userId: userId,
          status: {
            $in: [
              OrderStatus.searching,
              OrderStatus.inProgress,
              OrderStatus.noResponce,
              OrderStatus.waitingResponce,
            ],
          },
        },
      },
      {
        $lookup: {
          from: 'Driver',
          localField: 'driverId',
          foreignField: '_id',
          as: 'driver',
        },
      },
      {
        $addFields: {
          driver: {
            $first: '$driver',
          },
        },
      },
    ]);
    const results = await curOrders.toArray();

    curOrders.close();
    return results;
  }

  async notifyClient(order: OrderDB) {
    this.getOrders(order.userId)
      .then((val) => {
        this.listenUserMap
          .get(order.userId.toHexString())
          ?.forEach((subject) => {
            subject.next({
              orders: val.map(
                (val): proto.Order => ({
                  driverId: val.driverId?.toHexString(),
                  id: val._id.toHexString(),
                  status: val.status,
                  userId: val.userId.toHexString(),
                  pointOrders: val.pointOrders.map((val) =>
                    parsePointOrderDbToPointOrder(val),
                  ),
                  driver:
                    val.driver == null
                      ? undefined
                      : {
                          id: val.driver!._id.toHexString(),
                          position: parsePointOrderDbToLantLng(
                            val.driver?.position,
                          ),
                          status: val.driver!.status,
                          userId: val.userId.toHexString(),
                        },
                }),
              ),
            });
          });
      })
      .catch((e) => {
        console.log(e);
      });
    // this.listenUserMap
    //   .get(order.userId.toHexString())
    //   ?.forEach((singlSub) => {
    //     singlSub
    //   });

    try {
      const tokensCur = this.userCollection.aggregate<UserDBWithTokens>([
        {
          $match: {
            _id: order.userId,
          },
        },
        {
          $lookup: {
            from: 'Token',
            localField: '_id',
            foreignField: 'userId',
            as: 'tokens',
          },
        },
      ]);
      const tokens = (await tokensCur.next())?.tokens.filter(
        (val) => val.fcmToken != null,
      );

      tokensCur.close();
    } catch (e) {
      console.log(e);
    }
  }

  async notifyUserAboutAllOrders(userId: ObjectId): Promise<void> {
    this.getOrders(new ObjectId(userId))
      .then((val) => {
        const subjects = this.listenUserMap.get(userId.toHexString());
        const orders = val.map(
          (val): proto.Order => ({
            driverId: val.driverId?.toHexString(),
            id: val._id.toHexString(),
            status: val.status,
            userId: val.userId.toHexString(),
            pointOrders: val.pointOrders.map((val) =>
              parsePointOrderDbToPointOrder(val),
            ),

            driver:
              val.driver == null
                ? undefined
                : {
                    id: val.driver!._id.toHexString(),
                    position: parsePointOrderDbToLantLng(val.driver?.position),
                    status: val.driver!.status,
                    userId: val.userId.toHexString(),
                  },
          }),
        );
        subjects?.forEach((subject) => {
          subject.next({
            orders,
          });
        });
      })
      .catch((e) => {
        console.log(e);
      });
  }
}
