import { Inject, Injectable } from '@nestjs/common';
import { DriverDB } from 'src/modules/driver/model/db/driver_db';
import { OrderDB } from '../../model/order_db';
import { Collection, Db, ObjectId } from 'mongodb';
import { DriverStatus, OrderStatus } from 'src/proto/gtaxi';
import { ClientOrderService } from '../client_order.service';
import { IMongoService } from 'src/modules/mongo/mongo.module';
import { DriverOrderService } from '../driver_order.service';

@Injectable()
export class DriverOrderChangeService {
  driverCollection: Collection<DriverDB>;
  orderCollection: Collection<OrderDB>;

  constructor(
    private mongoSevice: IMongoService,
    private clientOrderService: ClientOrderService,
    private driverOrderService: DriverOrderService,
  ) {
    const database = this.mongoSevice.database;
    this.orderCollection = database.collection('Order');
    this.driverCollection = database.collection('Driver');
  }

  async acceptOrder(__: { orderId: ObjectId }) {
    const { orderId } = __;
    const newOrder = (await this.orderCollection.findOneAndUpdate(
      {
        _id: orderId,
      },
      {
        $set: {
          status: OrderStatus.inProgress,
        },
      },
      {
        returnDocument: 'after',
      },
    ))!;
    await this.driverCollection.findOneAndUpdate(
      {
        _id: newOrder.driverId!,
      },
      {
        $set: {
          status: DriverStatus.busy,
        },
      },
      {
        returnDocument: 'after',
      },
    );
    this.clientOrderService.notifyClient(newOrder);
    this.driverOrderService.notifyDriver(newOrder);
  }

  async declineOrder(__: { orderId: ObjectId }) {
    const { orderId } = __;
    const orderDb = await this.orderCollection.findOne({
      _id: orderId,
    });
    if (orderDb == null) throw 'No Order';
    const newDeclinedDriverIds = [...(orderDb?.declinedDriverIds ?? [])];
    if (orderDb.driverId != null) {
      await this.driverCollection.updateOne(
        {
          _id: orderDb.driverId,
        },
        {
          $set: {
            status: DriverStatus.active,
          },
        },
      );
      newDeclinedDriverIds.push(orderDb?.driverId);
    }

    const newOrder = (await this.orderCollection.findOneAndUpdate(
      {
        _id: orderId,
      },
      {
        $set: {
          status: OrderStatus.searching,
          driverId: undefined,
          declinedDriverIds: newDeclinedDriverIds,
        },
      },
      {
        returnDocument: 'after',
      },
    ))!;

    this.clientOrderService.notifyClient(newOrder);
    if (orderDb.driverId != null) {
      this.driverOrderService.notifyDriverAboutAllOrders(orderDb.driverId);
    }
    return newOrder;
  }
}
