import { Inject, Injectable } from '@nestjs/common';
import { Collection, Db, ObjectId } from 'mongodb';
import { OrderDB } from '../../model/order_db';
import * as proto from 'src/proto/gtaxi';
import { SingleFinderDriver } from '../single_finder_driver.service';
import { IMongoService } from 'src/modules/mongo/mongo.module';
import { ClientOrderService } from '../client_order.service';
import {
  parsePointOrderDbToLantLng,
  parsePointOrderDbToPointOrder,
} from 'src/utils/parse_mongo_geo_to_latlng';

@Injectable()
export class CreateOrderAsUserService {
  orderCollection: Collection<OrderDB>;

  constructor(
    private mongoSevice: IMongoService,
    private driverFinder: SingleFinderDriver,
    private clientOrderService: ClientOrderService,
  ) {
    const database = this.mongoSevice.database;
    this.orderCollection = database.collection('Order');
  }
  // you must be sure that clientPoints contains at least 2 points
  async createOrder(__: {
    userId: ObjectId;
    clientPoints: proto.PointOrder[];
  }): Promise<OrderDB> {
    const { clientPoints, userId } = __;
    const createdOrder: OrderDB = {
      _id: new ObjectId(),
      pointOrders: clientPoints.map((val) => ({
        type: val.type!,
        point: {
          type: 'Point',
          coordinates: [val.point!.longitude!, val.point!.latitude!],
        },
      })),
      declinedDriverIds: [],
      status: proto.OrderStatus.searching,
      userId: userId,
    };
    await this.orderCollection.insertOne(createdOrder);
    this.driverFinder.findDriver(createdOrder);
    this.clientOrderService.notifyUserAboutAllOrders(userId);
    return createdOrder;
  }
}
