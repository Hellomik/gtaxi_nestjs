import { HttpService } from '@nestjs/axios';
import { Inject, Injectable } from '@nestjs/common';
import { Collection, Db } from 'mongodb';
import { DriverDB } from 'src/modules/driver/model/db/driver_db';
import { OrderDB } from 'src/modules/order/model/order_db';
import { IMongoService, mongoClient } from 'src/modules/mongo/mongo.module';
import { Driver, DriverStatus, OrderStatus, PointType } from 'src/proto/gtaxi';
import { DriverOrderService } from './driver_order.service';
import { ClientOrderService } from './client_order.service';

@Injectable()
export class SingleFinderDriver {
  driverCollection: Collection<DriverDB>;
  orderCollection: Collection<OrderDB>;

  constructor(
    private mongoService: IMongoService,
    private httpService: HttpService,
    private driverOrderService: DriverOrderService,
    private clientOrderService: ClientOrderService,
  ) {
    const database = this.mongoService.database;
    this.orderCollection = database.collection('Order');
    this.driverCollection = database.collection('Driver');
  }

  async requestMatrixApi(__: {
    sources: { lat: number; lon: number }[];
    targets: { lat: number; lon: number }[];
  }): Promise<
    { distance: number; time: number; to_index: number; from_index: number }[][]
  > {
    const { sources, targets } = __;
    const responce = await this.httpService.axiosRef.get(
      'http://46.101.123.221:8002/sources_to_targets',
      {
        params: {
          json: JSON.stringify({
            sources: sources,
            targets: targets,
            costing: 'auto',
          }),
        },
      },
    );
    if (responce.status != 200) {
      throw 'Something went wrong';
    }
    return responce.data.sources_to_targets;
  }

  async findDriver(order: OrderDB) {
    const session = mongoClient.startSession();

    try {
      const updatedOrder = await session.withTransaction<OrderDB>(
        async (): Promise<OrderDB> => {
          const startPoint = order.pointOrders.find(
            (val) => val.type == PointType.start,
          );

          // TODO divide by sectors
          const cur = this.driverCollection.aggregate<DriverDB>(
            [
              {
                $geoNear: {
                  near: startPoint!.point,
                  distanceField: 'dist.calculated',
                  spherical: true,
                },
              },
              {
                $match: {
                  status: DriverStatus.active,
                },
              },
            ],
            {
              session,
            },
          );
          const drivers: DriverDB[] = await cur.toArray();
          cur.close();

          if (drivers.length == 0) {
            throw 'Driver not found';
          }
          const responce = await this.requestMatrixApi({
            sources: drivers.map((val) => ({
              lat: val.position!.coordinates[1],
              lon: val.position!.coordinates[0],
            })),
            targets: [
              {
                lon: startPoint!.point.coordinates[0],
                lat: startPoint!.point.coordinates[1],
              },
            ],
          });
          const matrix = responce[0];
          let minIndex: number = 0;
          for (let i = 1; i < matrix.length; ++i) {
            if (matrix[minIndex].distance > matrix[i].distance) {
              minIndex = i;
            }
          }
          const neededDriver = drivers[matrix[minIndex].from_index];
          const [dri, updatedOrder] = await Promise.all([
            this.driverCollection.updateOne(
              {
                _id: neededDriver._id,
              },
              {
                $set: {
                  status: DriverStatus.busy,
                },
              },
              {
                session,
              },
            ),
            this.orderCollection.findOneAndUpdate(
              {
                _id: order._id,
              },
              {
                $set: {
                  driverId: neededDriver._id,
                  status: OrderStatus.waitingResponce,
                },
              },
              {
                returnDocument: 'after',
                session,
              },
            ),
          ]);
          return updatedOrder!;
        },
        {
          readPreference: 'primary',
          readConcern: 'local',
          writeConcern: {
            w: 'majority',
          },
        },
      );
      await session.endSession();
      this.driverOrderService.notifyDriver(updatedOrder);
      this.clientOrderService.notifyClient(updatedOrder);
    } catch (e) {
      session.endSession();
      console.log(e);
    }
  }
}
