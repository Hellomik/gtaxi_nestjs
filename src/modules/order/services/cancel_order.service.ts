import { Injectable } from '@nestjs/common';
import { Collection, ObjectId } from 'mongodb';
import { IMongoService } from 'src/modules/mongo/mongo.module';
import { OrderDB } from '../model/order_db';
import { DriverDB } from 'src/modules/driver/model/db/driver_db';
import { UserDB } from 'src/modules/user/model/db/user_db';
import { ClientOrderService } from './client_order.service';
import { DriverOrderService } from './driver_order.service';
import { DriverStatus, OrderStatus } from 'src/proto/gtaxi';

@Injectable()
export class CancelOrderService {
  orderCollection: Collection<OrderDB>;
  driverCollection: Collection<DriverDB>;
  userCollection: Collection<UserDB>;

  constructor(private mongoService: IMongoService) {
    this.orderCollection = this.mongoService.database.collection('Order');
    this.driverCollection = this.mongoService.database.collection('Driver');
    this.userCollection = this.mongoService.database.collection('User');
  }

  async cancelOrder(__: {
    orderId: ObjectId;
    userId: ObjectId;
  }): Promise<OrderDB> {
    const { orderId, userId } = __;
    const foundedOrder = await this.orderCollection.findOneAndUpdate(
      {
        _id: orderId,
        userId: userId,
        // TODO add order status find
      },
      {
        $set: {
          status: OrderStatus.UNRECOGNIZED,
        },
      },
      {
        returnDocument: 'after',
      },
    );
    if (foundedOrder == null) {
      throw 'Order not found';
    }

    if (foundedOrder?.driverId) {
      await this.driverCollection.updateOne(
        {
          _id: foundedOrder.driverId!,
        },
        {
          $set: {
            status: DriverStatus.active,
          },
        },
      );
    }
    return foundedOrder!;
  }
}
