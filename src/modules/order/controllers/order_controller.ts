import { Metadata } from '@grpc/grpc-js';
import { Controller, UseFilters } from '@nestjs/common';
import { Observable, Subject } from 'rxjs';
import { GRPCUser } from 'src/modules/user/services/grpc_auth_guard';
import * as proto from 'src/proto/gtaxi';

import { ShieldDecorator } from 'src/utils/shield_decorator';
import { ClientOrderService } from '../services/client_order.service';
import { ObjectId } from 'mongodb';
import {
  parsePointOrderDbToLantLng,
  parsePointOrderDbToPointOrder,
} from 'src/utils/parse_mongo_geo_to_latlng';
import { parserDriverDbToProto } from 'src/modules/driver/model/parsers/driver_parse';
import { Payload } from '@nestjs/microservices';
import { DriverOrderService } from '../services/driver_order.service';
import { JwtService } from '@nestjs/jwt';
import { Http2gRPCExceptionFilter } from 'src/utils/http2gRPCException.filter';
import { ServerDuplexStreamImpl } from '@grpc/grpc-js/build/src/server-call';
import { getUserIdByMetadata } from 'src/utils/getUserIdByMetadata';
import { CancelOrderService } from '../services/cancel_order.service';
import { DriverLocationStreamService } from 'src/modules/driver/services/drvier_location_stream.service';

@Controller()
@proto.OrderServiceControllerMethods()
export class OrderController implements proto.OrderServiceController {
  constructor(
    private clientOrderService: ClientOrderService,
    private driverOrderService: DriverOrderService,
    private jwtService: JwtService,
    private cancelOrderService: CancelOrderService,
    private driverLocationStreamService: DriverLocationStreamService,
  ) {
    // console.log(
    //   this.jwtService.decode(
    //     'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCIsImV4cGlyZXNJbiI6IjEwcyJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.iWgCU8qJdhQGpShwOCLmbeW5ibxPBLQwrcdiGZu2Yj8'
    //     // 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCIsImV4cGlyZXNJbiI6IjFzIn0.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.uhq0KaNvXo5fI3aSdPEZzWXfidBgI_oFJp0mAeuKO2g',
    //   ),
    // );
  }

  @ShieldDecorator()
  async cancelOrder(
    @Payload() request: proto.OrderIdInput,
    @GRPCUser() userId?: string,
  ): Promise<proto.OrdersResponce> {
    const foundedOrder = await this.cancelOrderService.cancelOrder({
      orderId: new ObjectId(request.id),
      userId: new ObjectId(userId),
    });
    this.clientOrderService.notifyClient(foundedOrder!);
    this.driverOrderService.notifyDriver(foundedOrder!);
    const allOrders = await this.clientOrderService.getOrders(
      new ObjectId(userId),
    );
    return {
      orders: allOrders.map(
        (val): proto.Order => ({
          driverId: val.driverId?.toHexString(),
          id: val._id.toHexString(),
          status: val.status,
          userId: val.userId.toHexString(),
          pointOrders: val.pointOrders.map((val) =>
            parsePointOrderDbToPointOrder(val),
          ),
        }),
      ),
    };
  }

  public get driverMap(): Map<string, Subject<any>[]> {
    return this.driverOrderService.listenDriverMap;
  }
  public get userMap(): Map<string, Subject<any>[]> {
    return this.clientOrderService.listenUserMap;
  }

  async driverUpdate(message: proto.UpdateLocationInput, userId: ObjectId) {
    if (message.latLng?.longitude == null || message.latLng?.latitude == null) {
      return;
    }
    try {
      const driver =
        await this.driverOrderService.driverCollection.findOneAndUpdate(
          {
            userId,
          },
          {
            $set: {
              position: {
                type: 'Point',
                coordinates: [
                  message.latLng.longitude,
                  message.latLng.latitude,
                ],
              },
            },
          },
          {
            returnDocument: 'after',
          },
        );
      if (driver != null) {
        this.driverLocationStreamService.notifyDriverListeners(driver);
      }
    } catch (e) {
      console.log(e);
    }
  }

  @UseFilters(new Http2gRPCExceptionFilter())
  listenDriver(
    messages: Observable<proto.UpdateLocationInput>,
    metaData?: Metadata,
    duplex?: ServerDuplexStreamImpl<
      proto.UpdateLocationInput,
      proto.OrdersResponce
    >,
  ): Observable<proto.OrdersResponce> {
    const userId = getUserIdByMetadata(
      metaData!,
      this.jwtService,
    ).toHexString();

    if (userId == null) {
      throw 'UserId not found';
    }
    const subject = new Subject<proto.OrdersResponce>();
    const oldSubjects = this.driverMap.get(userId) ?? [];
    this.driverMap.set(userId, [...oldSubjects, subject]);
    const onNext = (message: proto.UpdateLocationInput) => {
      console.log('WELL I Received');
      this.driverUpdate(message, new ObjectId(userId));
    };
    const onComplete = () => {
      subject.complete();
      const oldSubjects = this.driverMap.get(userId) ?? [];
      const newSubjects = oldSubjects.filter((val) => val != subject);
      if (newSubjects.length) {
        this.driverMap.set(userId, newSubjects);
        return;
      }
      this.driverMap.delete(userId);
    };
    messages.subscribe({
      next: onNext,
      complete: onComplete,
    });
    this.driverOrderService
      .getOrders(new ObjectId(userId))
      .then((val) => {
        subject.next({
          orders: val.map(
            (val): proto.Order => ({
              driverId: val.driverId?.toHexString(),
              id: val._id.toHexString(),
              status: val.status,
              userId: val.userId.toHexString(),
              pointOrders: val.pointOrders.map((val) =>
                parsePointOrderDbToPointOrder(val),
              ),
            }),
          ),
        });
      })
      .catch((e) => {
        console.log(e);
      });

    return subject.asObservable();
  }

  @UseFilters(new Http2gRPCExceptionFilter())
  listenUser(
    messages: Observable<proto.EmptyBody>,
    metaData?: Metadata,
    duplex?: ServerDuplexStreamImpl<
      proto.UpdateLocationInput,
      proto.OrdersResponce
    >,
  ): Observable<proto.OrdersResponce> {
    const userId = getUserIdByMetadata(
      metaData!,
      this.jwtService,
    ).toHexString();

    if (userId == null) {
      throw 'UserId not found';
    }
    const sendOrder = () => {
      this.clientOrderService
        .getOrders(new ObjectId(userId))
        .then((val) => {
          subject.next({
            orders: val.map(
              (val): proto.Order => ({
                driverId: val.driverId?.toHexString(),
                id: val._id.toHexString(),
                status: val.status,
                userId: val.userId.toHexString(),
                pointOrders: val.pointOrders.map((val) =>
                  parsePointOrderDbToPointOrder(val),
                ),

                driver:
                  val.driver == null
                    ? undefined
                    : {
                        id: val.driver!._id.toHexString(),
                        position: parsePointOrderDbToLantLng(
                          val.driver?.position,
                        ),
                        status: val.driver!.status,
                        userId: val.userId.toHexString(),
                      },
              }),
            ),
          });
        })
        .catch((e) => {
          console.log(e);
        });
    };

    const subject = new Subject<proto.OrdersResponce>();
    const oldSubjects = this.userMap.get(userId) ?? [];
    this.userMap.set(userId, [...oldSubjects, subject]);
    const onNext = (message: proto.EmptyBody) => {
      sendOrder();
    };
    const onComplete = () => {
      // console.log('COMPL');
      subject.complete();
      const oldSubjects = this.userMap.get(userId) ?? [];
      const newSubjects = oldSubjects.filter((val) => val != subject);
      if (newSubjects.length) {
        this.userMap.set(userId, newSubjects);
        return;
      }
      this.userMap.delete(userId);
    };
    messages.subscribe({
      next: onNext,
      complete: onComplete,
    });
    sendOrder();

    return subject.asObservable();
  }

  @ShieldDecorator()
  async getOrderOfDriver(
    @Payload() request: proto.EmptyBody,
    @GRPCUser() userId?: string,
  ): Promise<proto.OrdersResponce> {
    throw new Error('Method not implemented.');
  }

  @ShieldDecorator()
  async getOrderOfClient(
    @Payload() request: proto.EmptyBody,
    @GRPCUser() userId?: string,
  ): Promise<proto.OrdersResponce> {
    const orders = await this.clientOrderService.getOrders(
      new ObjectId(userId),
    );
    return {
      orders: orders.map((e): proto.Order => {
        return {
          id: e._id.toHexString(),
          pointOrders: e.pointOrders?.map(
            (val): proto.PointOrder => parsePointOrderDbToPointOrder(val),
          ),
          status: e.status,
          driver: parserDriverDbToProto(e.driver),
          driverId: e.driverId?.toHexString(),
          userId: e.userId.toHexString(),
        };
      }),
    };
  }
}
