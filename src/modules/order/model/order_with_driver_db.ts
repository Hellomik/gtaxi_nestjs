import { DriverDB } from 'src/modules/driver/model/db/driver_db';
import { OrderDB } from 'src/modules/order/model/order_db';

export interface OrderWithDriverDb extends OrderDB {
  driver?: DriverDB;
}
