import { ObjectId } from 'mongodb';
import { PointOrderDB } from '../../driver/model/db/point_order_db';
import { OrderStatus } from 'src/proto/gtaxi';

export interface OrderDB {
  _id: ObjectId;
  pointOrders: PointOrderDB[];
  status: OrderStatus;
  userId: ObjectId;
  driverId?: ObjectId;
  declinedDriverIds: ObjectId[];
}
