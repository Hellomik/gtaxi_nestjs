import { Module, forwardRef } from '@nestjs/common';
import { SingleFinderDriver } from './services/single_finder_driver.service';
import { OrderFindDriver } from './services/order_find_driver.service';
import { DriverModule } from '../driver/driver.module';
import { ClientOrderService } from './services/client_order.service';
import { CreateOrderAsUserService } from './services/user/create_order_as_user.service';
import { OrderController } from './controllers/order_controller';
import { DriverOrderService } from './services/driver_order.service';
import { DriverOrderChangeService } from './services/user/driver_order_change.service';
import { CancelOrderService } from './services/cancel_order.service';

@Module({
  imports: [forwardRef(() => DriverModule)],
  providers: [
    SingleFinderDriver,
    OrderFindDriver,
    ClientOrderService,
    CreateOrderAsUserService,
    DriverOrderService,
    DriverOrderChangeService,
    CancelOrderService,
  ],
  controllers: [OrderController],
  exports: [
    SingleFinderDriver,
    CreateOrderAsUserService,
    DriverOrderChangeService,
  ],
})
export class OrderModule {}
