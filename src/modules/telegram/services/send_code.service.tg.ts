import { Inject, Injectable } from '@nestjs/common';
import { Collection, Db, ObjectId } from 'mongodb';
import { telegramBot } from './initialize_service';
import { cleanPhoneNumber } from 'src/utils/clean_phone_number';
import { ConfigService } from '@nestjs/config';
import { CodeService } from 'src/modules/user/services/code.service';
import { IMongoService } from 'src/modules/mongo/mongo.module';

@Injectable()
export class SendCodeTelegramService {
  telegramCollection: Collection<{
    _id: ObjectId;
    phoneNumber: string;
    chatId: number;
  }>;

  constructor(
    
    private mongoSevice: IMongoService,
    private codeSerivce: CodeService,
    private configService: ConfigService,
  ) {
    //   const isProd = this.configService.get('prod');
    //   this.telegramCollection = isProd
    //     ? this.database.collection('telegrambot')
    //     : this.database.collection('telegrambotTest');
    const database = this.mongoSevice.database;
    this.telegramCollection = database.collection('telegrambot');
  }

  async sendCode(__: { phoneNumber: string }): Promise<boolean> {
    const phoneNumber = cleanPhoneNumber(__.phoneNumber);
    // const entity = await this.telegramCollection.findOne({
    //   phoneNumber: phoneNumber,
    // });
    // if (entity == null) return false;
    const verifyCode = this.codeSerivce.generateCode(phoneNumber);
    // telegramBot.sendMessage(
    //   entity.chatId,
    //   `Можете продолжить верификацию в приложении.\nВаш код верификации ${verifyCode} для номера +${phoneNumber}`,
    // );
    return true;
  }
}
