import { Inject, Injectable } from '@nestjs/common';
import { Collection, Db, ObjectId } from 'mongodb';
import * as TelegramBot from 'node-telegram-bot-api';

import { cleanPhoneNumber } from 'src/utils/clean_phone_number';
import { CodeService } from 'src/modules/user/services/code.service';
import { IMongoService } from 'src/modules/mongo/mongo.module';

const accessTokenTelegram = '6850334143:AAGgju2mmjX8Zsb5uCfjazMMCrL7DKC0tn8';

export const telegramBot = new TelegramBot(accessTokenTelegram, {
  // polling: true,
});

@Injectable()
export class InitializeService {
  telegramCollection: Collection<{
    _id: ObjectId;
    phoneNumber: string;
    chatId: number;
  }>;

  constructor(
    private mongoSevice: IMongoService,
    private codeSerivce: CodeService,
  ) {
    const database = mongoSevice.database;
    this.telegramCollection = database.collection('telegrambot');
    this.init();
  }

  async onMessage(
    message: TelegramBot.Message,
    metadata: TelegramBot.Metadata,
  ) {
    try {
      if (message.text == '/start') {
        this.setKeyboard(message, metadata);
        return;
      }
      if (metadata.type == 'contact') {
        if (message.contact.user_id != message.from.id) {
          telegramBot.sendMessage(message.chat.id, `Это не ваш номер телефона`);
          return;
        }
        const phoneNumber = cleanPhoneNumber(message.contact.phone_number);
        await this.telegramCollection.updateOne(
          {
            phoneNumber,
          },
          {
            $set: {
              phoneNumber,
              chatId: message.chat.id,
            },
          },
          { upsert: true },
        );
        const verifyCode = this.codeSerivce.generateCode(phoneNumber);
        telegramBot
          .sendMessage(
            message.chat.id,
            `Можете продолжить верификацию в приложении.\nВаш код верификации ${verifyCode} для номера +${phoneNumber}`,
          )
          .catch((e) => console.log(e));
      }
    } catch (e) {
      console.log(e);
    }
  }

  async setKeyboard(
    message: TelegramBot.Message,
    metadata: TelegramBot.Metadata,
  ) {
    try {
      await telegramBot.sendMessage(
        message.chat.id,
        'Как мы можем с вами связаться ?',
        {
          parse_mode: 'Markdown',
          reply_markup: {
            one_time_keyboard: true,
            keyboard: [
              [
                {
                  text: 'Номер телефона',
                  request_contact: true,
                },
              ],
            ],
          },
        },
      );
    } catch (e) {
      console.log(e);
    }
  }

  async init() {
    telegramBot.on(
      'message',
      (message: TelegramBot.Message, metadata: TelegramBot.Metadata) =>
        this.onMessage(message, metadata),
    );
  }
}
