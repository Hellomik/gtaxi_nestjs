import { ObjectId } from 'mongodb';
import { DriverStatus, LatLng } from 'src/proto/gtaxi';
import { MongoPoint } from 'src/utils/mongo_point';

export interface DriverDB {
  _id: ObjectId;
  status: DriverStatus;
  userId: ObjectId;
  ///                                      long    lat
  position?: MongoPoint;
}
