import { PointType } from 'src/proto/gtaxi';
import { MongoPoint } from 'src/utils/mongo_point';

export interface PointOrderDB {
  point: MongoPoint;
  type: PointType;
}
