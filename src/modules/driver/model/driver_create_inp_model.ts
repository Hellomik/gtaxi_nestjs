import { IsString } from 'class-validator';
import { DriverCreateInput } from 'src/proto/gtaxi';

export class DriverCreateInputModel implements DriverCreateInput {
  @IsString()
  car: string;

  @IsString()
  userId: string;
}
