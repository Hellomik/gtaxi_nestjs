import { Driver } from 'src/proto/gtaxi';
import { DriverDB } from '../db/driver_db';

export const parserDriverDbToProto = (
  driverDb?: DriverDB,
): Driver | undefined => {
  if (driverDb == null) return undefined;
  return {
    id: driverDb._id.toHexString(),
    position:
      driverDb.position == null
        ? undefined
        : {
            longitude: driverDb.position.coordinates[0],
            latitude: driverDb.position.coordinates[1],
          },
    status: driverDb.status,
    userId: driverDb.userId.toHexString()
  };
};
