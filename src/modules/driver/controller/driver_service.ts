import {
  Controller,
  Inject,
  UseFilters,
  UseGuards,
  UsePipes,
  ValidationPipe,
} from '@nestjs/common';

import { Observable, Subject } from 'rxjs';

import * as proto from 'src/proto/gtaxi';
import { Http2gRPCExceptionFilter } from 'src/utils/http2gRPCException.filter';
import { DriverCreateInputModel } from '../model/driver_create_inp_model';
import { HttpService } from '@nestjs/axios';
import { Metadata } from '@grpc/grpc-js';
import {
  GRPCUser,
  GrpcAuthGuard,
} from 'src/modules/user/services/grpc_auth_guard';
import { Collection, Db, ObjectId } from 'mongodb';

import { OrderDB } from '../../order/model/order_db';
import { SingleFinderDriver } from 'src/modules/order/services/single_finder_driver.service';
import { DriverDB } from '../model/db/driver_db';
import { IMongoService } from 'src/modules/mongo/mongo.module';
import { ShieldDecorator } from 'src/utils/shield_decorator';
import { Payload, RpcException } from '@nestjs/microservices';
import { parserDriverDbToProto } from '../model/parsers/driver_parse';
import { ServerDuplexStreamImpl } from '@grpc/grpc-js/build/src/server-call';
import { getUserIdByMetadata } from 'src/utils/getUserIdByMetadata';
import { JwtService } from '@nestjs/jwt';
import { DriverLocationStreamService } from '../services/drvier_location_stream.service';
// DriverStatus;

@Controller()
@proto.DriverServiceControllerMethods()
export class DriverController implements proto.DriverServiceController {
  driverCollection: Collection<DriverDB>;
  orderCollection: Collection<OrderDB>;

  constructor(
    private httpService: HttpService,
    private mongoService: IMongoService,
    private singleFinderDriver: SingleFinderDriver,
    private jwtService: JwtService,
    private driverLocationStreamService: DriverLocationStreamService,
  ) {
    const database = this.mongoService.database;
    this.orderCollection = database.collection('Order');
    this.driverCollection = database.collection('Driver');
  }

  @UseFilters(new Http2gRPCExceptionFilter())
  getDriverLocationStream(
    messages: Observable<proto.DriverIdInput>,
    metaData?: Metadata,
    duplex?: ServerDuplexStreamImpl<proto.DriverIdInput, proto.DriverResponce>,
  ): Observable<proto.DriverResponce> {
    const userId = getUserIdByMetadata(
      metaData!,
      this.jwtService,
    ).toHexString();
    if (userId == null) {
      throw 'UserId not found';
    }
    const subject = new Subject<proto.DriverResponce>();

    this.driverLocationStreamService.addListener(messages, subject);
    return subject.asObservable();
  }

  // @ShieldDecorator()
  async getDriverLocation(
    request: proto.DriverIdInput,
  ): Promise<proto.DriverResponce> {
    const cur = this.driverCollection.find({
      _id: {
        $in: (request.driverIds ?? []).map((e) => new ObjectId(e)),
      },
    });
    const drivers = await cur.toArray();
    cur.close();

    return {
      drivers: drivers.map((val) => parserDriverDbToProto(val)!),
    };
  }

  @ShieldDecorator()
  async getMySelf(
    @Payload() request: proto.EmptyBody,
    @GRPCUser() userId?,
  ): Promise<proto.Driver> {
    const userFound = await this.driverCollection.findOne({
      userId: new ObjectId(userId),
    });

    if (userFound == null) throw new RpcException('Not Driver');

    return {
      id: userFound._id.toHexString(),
      status: userFound.status,
      userId: userFound.userId.toHexString(),
    };
  }

  @UsePipes(new ValidationPipe({ transform: true }))
  @UseFilters(new Http2gRPCExceptionFilter())
  async createDriver(request: DriverCreateInputModel): Promise<proto.Driver> {
    const userFound = await this.driverCollection.findOne({
      userId: new ObjectId(request.userId),
    });

    if (userFound != null) throw 'Driver Exist';
    const createdDriver = {
      _id: new ObjectId(),
      status: proto.DriverStatus.passive,
      userId: new ObjectId(request.userId),
    };
    await this.driverCollection.insertOne(createdDriver);

    return {
      id: createdDriver._id.toHexString(),
      status: createdDriver.status,
      userId: createdDriver.userId.toHexString(),
    };
  }

  // @UsePipes(new ValidationPipe({ transform: true }))
  // @UseFilters(new Http2gRPCExceptionFilter())
  // driverLocation(
  //   upStream: Observable<proto.DriverUpdateLocation>,
  // ): Observable<proto.LatLng> {
  //   const subject = new Subject<proto.LatLng>();

  //   const onNext = (message: proto.DriverUpdateLocation) => {
  //     subject.next({
  //       latitude: message.latLng.latitude,
  //       longitude: message.latLng.longitude,
  //     });
  //   };
  //   upStream.subscribe({
  //     next: onNext,
  //     complete: () => subject.complete(),
  //   });

  //   return subject.asObservable();
  // }

  // listerORderSubjectMap: Map<string, Subject<any>[]> = new Map();

  // @UseGuards(GrpcAuthGuard)
  // @UsePipes(new ValidationPipe({ transform: true }))
  // @UseFilters(new Http2gRPCExceptionFilter())
  // listenOrders(
  //   messages: Observable<proto.EmptyResponce>,
  //   header?: Metadata,
  //   @GRPCUser() userId?: string,
  // ): Observable<proto.Order> {
  //   const subject = new Subject<proto.Order>();
  //   const oldSubjects = this.listerORderSubjectMap.get(userId) ?? [];
  //   this.listerORderSubjectMap.set(userId, [...oldSubjects, subject]);
  //   const onNext = (message) => {};
  //   const onComplete = () => {
  //     subject.complete();
  //     const oldSubjects = this.listerORderSubjectMap.get(userId) ?? [];
  //     const newSubjects = oldSubjects.filter((val) => val != subject);
  //     if (newSubjects.length) {
  //       this.listerORderSubjectMap.set(userId, newSubjects);
  //       return;
  //     }
  //     this.listerORderSubjectMap.delete(userId);
  //   };
  //   messages.subscribe({
  //     next: onNext,
  //     complete: onComplete,
  //   });

  //   return subject.asObservable();
  // }
}
