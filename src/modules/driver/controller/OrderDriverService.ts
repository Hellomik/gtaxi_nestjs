import { Controller } from '@nestjs/common';
import { ObjectId } from 'mongodb';
import { Observable } from 'rxjs';
import { DriverOrderChangeService } from 'src/modules/order/services/user/driver_order_change.service';

import * as proto from 'src/proto/gtaxi';
import { parsePointOrderDbToPointOrder } from 'src/utils/parse_mongo_geo_to_latlng';

@Controller()
@proto.OrderDriverServiceControllerMethods()
export class OrderDriverController
  implements proto.OrderDriverServiceController
{
  constructor(private driverOrderChangeService: DriverOrderChangeService) {}

  async declineOrder(
    request: proto.OrderIdInput,
  ): Promise<proto.OrdersResponce> {
    const order = await this.driverOrderChangeService.declineOrder({
      orderId: new ObjectId(request.id),
    });
    return {
      // TODO
      orders: [],
    };
  }
  async acceptOrder(
    request: proto.OrderIdInput,
  ): Promise<proto.OrdersResponce> {
    const orders = await this.driverOrderChangeService.acceptOrder({
      orderId: new ObjectId(request.id),
    });
    return {
      // TODO
      orders: [],
    };
  }
}
