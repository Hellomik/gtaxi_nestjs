import { Module, forwardRef } from '@nestjs/common';
import { NearestService } from './services/nearest.service';
import { DriverController } from './controller/driver_service';
import { OrderModule } from '../order/order.module';
import { DriverService } from './services/driver.service';
import { OrderDriverController } from './controller/OrderDriverService';
import { DriverLocationStreamService } from './services/drvier_location_stream.service';

@Module({
  imports: [forwardRef(() => OrderModule)],
  controllers: [DriverController, OrderDriverController],
  providers: [NearestService, DriverService, DriverLocationStreamService],
  exports: [DriverLocationStreamService],
})
export class DriverModule {}
