// import { Injectable } from '@nestjs/common';
// import { Observable } from 'rxjs';

import { Inject, Injectable } from '@nestjs/common';
import { OrderDB } from '../../order/model/order_db';
import { Collection, Db } from 'mongodb';
import { DriverDB } from '../model/db/driver_db';
import { UserDB } from 'src/modules/user/model/db/user_db';
import { TokenDB } from 'src/modules/user/model/db/token_db';
import { IMongoService } from 'src/modules/mongo/mongo.module';

// @Injectable()
// export class DriverService {
//   private observableForEndpoint: Map<string, Observable<Event>> = new Map();

//   sendEventToConnector(endpointId: string, event: Event): boolean {
//     const s = this.subjectForEndpoint.get(endpointId);
//     if (s) {
//       s.next(event);
//       return true;
//     } else {
//     }
//     return false;
//   }

//   subscribeEvents(request: EventSubscriptionRequest): Observable<Event> {
//     const self = this;

//     if (request.id) {
//       const existingObservable: Observable<Event> =
//         this.observableForEndpoint.get(request.id);

//       Logger.l.log('A new endpoint connected with ID: ' + request.id);
//       setTimeout(() => {
//         self.onNewEndpointConnectedSubscriber.next(request.id);
//       }, 500);

//       if (existingObservable) {
//         return existingObservable;
//       } else {
//         const subject = new Subject<Event>();
//         const observable = subject.asObservable();
//         this.subjectForEndpoint.set(request.id, subject);
//         this.observableForEndpoint.set(request.id, observable);
//         return observable;
//       }
//     } else {
//       return undefined;
//     }
//   }
// }

@Injectable()
export class DriverService {
  driverCollection: Collection<DriverDB>;
  orderCollection: Collection<OrderDB>;
  userCollection: Collection<UserDB>;

  constructor(private mongoService: IMongoService) {
    const database = this.mongoService.database;
    this.orderCollection = database.collection('Order');
    this.driverCollection = database.collection('Driver');
    this.userCollection = database.collection('User');
  }

  async notifyListener(order: OrderDB): Promise<void> {
    const cur = this.userCollection.aggregate<
      UserDB & {
        tokens: TokenDB[];
      }
    >([
      [
        {
          $match: {
            driverId: order.driverId,
          },
        },
        {
          $lookup: {
            from: 'Token',
            localField: '_id',
            foreignField: 'userId',
            as: 'tokens',
          },
        },
      ],
    ]);
  }
}
