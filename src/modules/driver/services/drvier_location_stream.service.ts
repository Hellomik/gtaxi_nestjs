import { Injectable } from '@nestjs/common';
import { Collection, ObjectId } from 'mongodb';
import { Observable, Subject } from 'rxjs';
import { IMongoService } from 'src/modules/mongo/mongo.module';
import { DriverIdInput, DriverResponce } from 'src/proto/gtaxi';
import { DriverDB } from '../model/db/driver_db';
import { parserDriverDbToProto } from '../model/parsers/driver_parse';
import { first } from 'rxjs/operators';

@Injectable()
export class DriverLocationStreamService {
  driverMap: Map<string, Set<string>> = new Map();

  listenMapDriverBridge: Map<string, Set<string>> = new Map();
  listenMap: Map<string, Subject<DriverResponce>> = new Map();

  driverCollection: Collection<DriverDB>;

  constructor(private mongoService: IMongoService) {
    const database = this.mongoService.database;
    this.driverCollection = database.collection('Driver');
  }

  async addListener(
    incomeObs: Observable<DriverIdInput>,
    outCome: Subject<DriverResponce>,
  ) {
    const idSubject = new ObjectId().toHexString();

    const cleanAll = () => {
      const driverArray = Array.from(
        this.listenMapDriverBridge.get(idSubject)!,
      );
      driverArray.forEach((driverId) => {
        this.driverMap.get(driverId)?.delete(idSubject);
      });
    };
    const inNext = (message: DriverIdInput) => {
      cleanAll();

      const setAll = () => {
        const driverIds: string[] = message.driverIds ?? [];
        this.listenMapDriverBridge.set(idSubject, new Set(driverIds));
        console.log('WTF');
        (driverIds ?? []).forEach((driverId) => {
          if (!this.driverMap.has(driverId)) {
            this.driverMap.set(driverId, new Set());
          }
          this.driverMap.get(driverId)!.add(idSubject);
        });
      };
      setAll();
    };

    const inComplete = () => {
      cleanAll();
      this.listenMap.get(idSubject)?.complete();
      this.listenMap.delete(idSubject);
      outCome.complete();
    };

    this.listenMap.set(idSubject, outCome);
    this.listenMapDriverBridge.set(idSubject, new Set());

    // const firstMessage = incomeObs.pipe(first());
    incomeObs.subscribe(async (message) => {
      const outPut = await this.getDriverLocation(message);
      this.listenMap.get(idSubject)?.next(outPut);
    });

    incomeObs.subscribe({
      next: inNext,
      complete: inComplete,
    });
  }

  async getDriverLocation(request: DriverIdInput): Promise<DriverResponce> {
    const cur = this.driverCollection.find({
      _id: {
        $in: (request.driverIds ?? []).map((e) => new ObjectId(e)),
      },
    });
    const drivers = await cur.toArray();
    cur.close();

    return {
      drivers: drivers.map((val) => parserDriverDbToProto(val)!),
    };
  }

  async notifyDriverListeners(driver: DriverDB) {
    const driverSet = Array.from(
      this.driverMap.get(driver._id.toHexString()) ?? [],
    );
    driverSet.map((val) => {
      this.listenMap.get(val)?.next({
        drivers: [parserDriverDbToProto(driver)!],
      });
    });
  }
}
