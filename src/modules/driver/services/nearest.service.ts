import { Inject, Injectable } from '@nestjs/common';
import { Collection, Db, Document } from 'mongodb';
import { IMongoService } from 'src/modules/mongo/mongo.module';

@Injectable()
export class NearestService {
  driverCollection: Collection;

  constructor(private mongoService: IMongoService) {
    const database = this.mongoService.database;
    this.driverCollection = database.collection('Driver');
  }

  async getData(__: { longitude: number; latitude: number }) {
    const aggrCur = this.driverCollection.aggregate([
      {
        $geoNear: {
          near: {
            type: 'Point',
            coordinates: [__.longitude, __.latitude],
          },
          distanceField: 'distance',
        },
      },
    ]);
    const drivers = await aggrCur.toArray();
    aggrCur.close();
    const driverMap: Record<number, Document[]> = {};
    for (let i = 0; i < drivers.length; ++i) {
      const longitude = drivers[i].position.coordinates[0];
      const latitude = drivers[i].position.coordinates[1];
      const angle = angleFromCoordinate(
        {
          lat1: __.latitude,
          long1: __.longitude,
        },
        {
          lat2: latitude,
          long2: longitude,
        },
      );
      (driverMap[Math.floor(angle / 60)] ??= []).push(drivers[i]);
    }
    const entries = Object.entries(driverMap);
    const pathRequest: Promise<any>[] = [];
    for (let i = 0; i < entries.length; ++i) {
      pathRequest.push(
        ...entries.splice(0, 3).map(async () => {
          // await axios.post('http://localhost:8989/route', {
          //   points: [riderSearching.position, driver.position],
          //   profile: 'car',
          //   elevation: true,
          //   debug: false,
          //   instructions: true,
          //   locale: 'en_US',
          //   optimize: 'false',
          //   points_encoded: true,
          //   snap_preventions: ['ferry'],
          //   details: [
          //     'road_class',
          //     'road_environment',
          //     'max_speed',
          //     'average_speed',
          //   ],
          // });
        }),
      );
    }
    const responce = await Promise.allSettled(pathRequest);
  }
}
