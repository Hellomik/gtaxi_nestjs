import { FactoryProvider, Global, Injectable, Module } from '@nestjs/common';
import { ConfigModule, ConfigService } from '@nestjs/config';

import { Collection, Db, MongoClient } from 'mongodb';

export let mongoClient: MongoClient;

@Injectable()
export class IMongoService {
  database: Db;
  constructor(private configService: ConfigService) {}

  async connect() {
    mongoClient = await MongoClient.connect(
      this.configService.get('DATABASE_URL')!,
    );
    this.database = mongoClient.db('gtaxi');
  }
}

export const mongodDbFactory: FactoryProvider<Promise<IMongoService>> = {
  provide: IMongoService,
  useFactory: async (configService: ConfigService): Promise<IMongoService> => {
    const mongoService = new IMongoService(configService);
    await mongoService.connect();
    return mongoService;
  },
  inject: [ConfigService],
};

@Global()
@Module({
  imports: [ConfigModule],
  providers: [mongodDbFactory],
  exports: [mongodDbFactory],
})
export class MongoDBModule {}
