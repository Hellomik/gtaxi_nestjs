/* eslint-disable */
import { GrpcMethod, GrpcStreamMethod } from "@nestjs/microservices";
import { Observable } from "rxjs";
import { Timestamp } from "../../google/protobuf/timestamp.js";

export const protobufPackage = "gtaxi";

export enum DriverStatus {
  active = 0,
  passive = 1,
  busy = 2,
  UNRECOGNIZED = -1,
}

export enum OrderStatus {
  searching = 0,
  inProgress = 1,
  waitingResponce = 2,
  finished = 3,
  noResponce = 4,
  waitingClient = 5,
  onRoad = 6,
  UNRECOGNIZED = -1,
}

export enum PointType {
  start = 0,
  route = 1,
  UNRECOGNIZED = -1,
}

export interface OrderIdInput {
  id?: string | undefined;
}

export interface OrdersResponce {
  orders?: Order[] | undefined;
}

export interface AuthPhoneNumberInput {
  phoneNumber?: string | undefined;
}

export interface PhoneNumberExist {
  phoneNumberExist?: boolean | undefined;
}

export interface AuthVerifyCodeInput {
  phoneNumber?: string | undefined;
  code?: string | undefined;
  fullName?: string | undefined;
  device?: Device | undefined;
}

export interface SendCodeTelegramResponce {
  codeSent?: boolean | undefined;
}

export interface Device {
  deviceId?: string | undefined;
  fcmToken?: string | undefined;
}

export interface EmptyBody {
}

export interface UserTokenResponce {
  user?: User | undefined;
  token?: Token | undefined;
  bornDay?: Timestamp | undefined;
}

export interface Token {
  accessToken?: string | undefined;
  refreshToken?: string | undefined;
}

export interface User {
  id?: string | undefined;
  email?: string | undefined;
  phoneNumber?: string | undefined;
}

export interface DriverCreateInput {
  car?: string | undefined;
  userId?: string | undefined;
}

export interface RequestDriverInput {
  points?: ClientPoint[] | undefined;
}

export interface LatLng {
  latitude?: number | undefined;
  longitude?: number | undefined;
}

export interface UpdateLocationInput {
  latLng?: LatLng | undefined;
}

export interface Driver {
  id?: string | undefined;
  position?: LatLng | undefined;
  status?: DriverStatus | undefined;
  userId?: string | undefined;
}

export interface ClientPoint {
  latLng?: LatLng | undefined;
}

export interface PointOrder {
  point?: LatLng | undefined;
  type?: PointType | undefined;
}

export interface Order {
  id?: string | undefined;
  status?: OrderStatus | undefined;
  pointOrders?: PointOrder[] | undefined;
  userId?: string | undefined;
  driverId?: string | undefined;
  driver?: Driver | undefined;
}

export interface DriverIdInput {
  driverIds?: string[] | undefined;
}

export interface DriverResponce {
  drivers?: Driver[] | undefined;
}

export const GTAXI_PACKAGE_NAME = "gtaxi";

export interface AuthServiceClient {
  sendCodeTelegram(request: AuthPhoneNumberInput): Observable<SendCodeTelegramResponce>;

  verifyCode(request: AuthVerifyCodeInput): Observable<UserTokenResponce>;
}

export interface AuthServiceController {
  sendCodeTelegram(
    request: AuthPhoneNumberInput,
  ): Promise<SendCodeTelegramResponce> | Observable<SendCodeTelegramResponce> | SendCodeTelegramResponce;

  verifyCode(
    request: AuthVerifyCodeInput,
  ): Promise<UserTokenResponce> | Observable<UserTokenResponce> | UserTokenResponce;
}

export function AuthServiceControllerMethods() {
  return function (constructor: Function) {
    const grpcMethods: string[] = ["sendCodeTelegram", "verifyCode"];
    for (const method of grpcMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(constructor.prototype, method);
      GrpcMethod("AuthService", method)(constructor.prototype[method], method, descriptor);
    }
    const grpcStreamMethods: string[] = [];
    for (const method of grpcStreamMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(constructor.prototype, method);
      GrpcStreamMethod("AuthService", method)(constructor.prototype[method], method, descriptor);
    }
  };
}

export const AUTH_SERVICE_NAME = "AuthService";

export interface DriverServiceClient {
  createDriver(request: DriverCreateInput): Observable<Driver>;

  getMySelf(request: EmptyBody): Observable<Driver>;

  getDriverLocation(request: DriverIdInput): Observable<DriverResponce>;

  getDriverLocationStream(request: Observable<DriverIdInput>): Observable<DriverResponce>;
}

export interface DriverServiceController {
  createDriver(request: DriverCreateInput): Promise<Driver> | Observable<Driver> | Driver;

  getMySelf(request: EmptyBody): Promise<Driver> | Observable<Driver> | Driver;

  getDriverLocation(request: DriverIdInput): Promise<DriverResponce> | Observable<DriverResponce> | DriverResponce;

  getDriverLocationStream(request: Observable<DriverIdInput>): Observable<DriverResponce>;
}

export function DriverServiceControllerMethods() {
  return function (constructor: Function) {
    const grpcMethods: string[] = ["createDriver", "getMySelf", "getDriverLocation"];
    for (const method of grpcMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(constructor.prototype, method);
      GrpcMethod("DriverService", method)(constructor.prototype[method], method, descriptor);
    }
    const grpcStreamMethods: string[] = ["getDriverLocationStream"];
    for (const method of grpcStreamMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(constructor.prototype, method);
      GrpcStreamMethod("DriverService", method)(constructor.prototype[method], method, descriptor);
    }
  };
}

export const DRIVER_SERVICE_NAME = "DriverService";

export interface UserServiceClient {
  createOrder(request: RequestDriverInput): Observable<Order>;

  userListenOrders(request: Observable<EmptyBody>): Observable<Order>;
}

export interface UserServiceController {
  createOrder(request: RequestDriverInput): Promise<Order> | Observable<Order> | Order;

  userListenOrders(request: Observable<EmptyBody>): Observable<Order>;
}

export function UserServiceControllerMethods() {
  return function (constructor: Function) {
    const grpcMethods: string[] = ["createOrder"];
    for (const method of grpcMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(constructor.prototype, method);
      GrpcMethod("UserService", method)(constructor.prototype[method], method, descriptor);
    }
    const grpcStreamMethods: string[] = ["userListenOrders"];
    for (const method of grpcStreamMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(constructor.prototype, method);
      GrpcStreamMethod("UserService", method)(constructor.prototype[method], method, descriptor);
    }
  };
}

export const USER_SERVICE_NAME = "UserService";

export interface OrderServiceClient {
  listenDriver(request: Observable<UpdateLocationInput>): Observable<OrdersResponce>;

  listenUser(request: Observable<EmptyBody>): Observable<OrdersResponce>;

  getOrderOfDriver(request: EmptyBody): Observable<OrdersResponce>;

  getOrderOfClient(request: EmptyBody): Observable<OrdersResponce>;

  cancelOrder(request: OrderIdInput): Observable<OrdersResponce>;
}

export interface OrderServiceController {
  listenDriver(request: Observable<UpdateLocationInput>): Observable<OrdersResponce>;

  listenUser(request: Observable<EmptyBody>): Observable<OrdersResponce>;

  getOrderOfDriver(request: EmptyBody): Promise<OrdersResponce> | Observable<OrdersResponce> | OrdersResponce;

  getOrderOfClient(request: EmptyBody): Promise<OrdersResponce> | Observable<OrdersResponce> | OrdersResponce;

  cancelOrder(request: OrderIdInput): Promise<OrdersResponce> | Observable<OrdersResponce> | OrdersResponce;
}

export function OrderServiceControllerMethods() {
  return function (constructor: Function) {
    const grpcMethods: string[] = ["getOrderOfDriver", "getOrderOfClient", "cancelOrder"];
    for (const method of grpcMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(constructor.prototype, method);
      GrpcMethod("OrderService", method)(constructor.prototype[method], method, descriptor);
    }
    const grpcStreamMethods: string[] = ["listenDriver", "listenUser"];
    for (const method of grpcStreamMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(constructor.prototype, method);
      GrpcStreamMethod("OrderService", method)(constructor.prototype[method], method, descriptor);
    }
  };
}

export const ORDER_SERVICE_NAME = "OrderService";

export interface OrderDriverServiceClient {
  declineOrder(request: OrderIdInput): Observable<OrdersResponce>;

  acceptOrder(request: OrderIdInput): Observable<OrdersResponce>;
}

export interface OrderDriverServiceController {
  declineOrder(request: OrderIdInput): Promise<OrdersResponce> | Observable<OrdersResponce> | OrdersResponce;

  acceptOrder(request: OrderIdInput): Promise<OrdersResponce> | Observable<OrdersResponce> | OrdersResponce;
}

export function OrderDriverServiceControllerMethods() {
  return function (constructor: Function) {
    const grpcMethods: string[] = ["declineOrder", "acceptOrder"];
    for (const method of grpcMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(constructor.prototype, method);
      GrpcMethod("OrderDriverService", method)(constructor.prototype[method], method, descriptor);
    }
    const grpcStreamMethods: string[] = [];
    for (const method of grpcStreamMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(constructor.prototype, method);
      GrpcStreamMethod("OrderDriverService", method)(constructor.prototype[method], method, descriptor);
    }
  };
}

export const ORDER_DRIVER_SERVICE_NAME = "OrderDriverService";
