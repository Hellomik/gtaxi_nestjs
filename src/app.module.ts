import { Module } from '@nestjs/common';
import { UserModule } from './modules/user/user.module';
import { ConfigModule } from '@nestjs/config';
import { MongoDBModule } from './modules/mongo/mongo.module';
import { ScheduleModule } from '@nestjs/schedule';
import { GrpcReflectionModule } from 'nestjs-grpc-reflection';

import { grpcClientOptions } from './utils/grpc_option';
import { TelegramModule } from './modules/telegram/telegram.module';
import { JwtModule } from '@nestjs/jwt';
import { DriverModule } from './modules/driver/driver.module';
import { HttpModule } from '@nestjs/axios';
import { OrderModule } from './modules/order/order.module';

@Module({
  imports: [
    ConfigModule.forRoot({
      isGlobal: true,
    }),
    JwtModule.register({
      global: true,
      signOptions: {
        expiresIn: '300d',
      },
      secret: 'dwa241dkwi1k2e1weq,e2,12wedql3456789wd',
    }),
    { ...HttpModule.register({}), global: true },
    MongoDBModule,
    { ...ScheduleModule.forRoot(), global: true },

    GrpcReflectionModule.register(grpcClientOptions),
    UserModule,
    TelegramModule,
    DriverModule,
    OrderModule,
    // GraphQLModule.forRoot<ApolloDriverConfig>({
    //   driver: ApolloDriver,
    //   playground: true,
    //   autoSchemaFile: true,
    //   sortSchema: true,
    // }),
    // DriverModule,
  ],

  providers: [],
})
export class AppModule {}
