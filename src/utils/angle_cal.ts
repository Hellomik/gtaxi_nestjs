function radians_to_degrees(radians) {
  var pi = Math.PI;
  return radians * (180 / pi);
}

const angleFromCoordinate = (
  point1: {
    lat1: number;
    long1: number;
  },
  point2: {
    lat2: number;
    long2: number;
  },
): number => {
  const { lat1, long1 } = point1;
  const { lat2, long2 } = point2;

  const dLon = long2 - long1;

  const y = Math.sin(dLon) * Math.cos(lat2);
  const x =
    Math.cos(lat1) * Math.sin(lat2) -
    Math.sin(lat1) * Math.cos(lat2) * Math.cos(dLon);

  let brng = Math.atan2(y, x);

  brng = radians_to_degrees(brng);
  brng = (brng + 360) % 360;
  brng = 360 - brng; // count degrees counter-clockwise - remove to make clockwise

  return brng;
};
