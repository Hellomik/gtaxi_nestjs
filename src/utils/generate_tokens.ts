import { JwtService } from '@nestjs/jwt';

export const generateTokensByUserId = (
  userId: String,
  jwtService: JwtService,
) => {
  return {
    accessToken: jwtService.sign({ id: userId }),
    refreshToken: jwtService.sign({ id: userId, expiresIn: '100d' }),
  };
};
