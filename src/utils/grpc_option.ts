import { Transport } from '@nestjs/microservices';
import { addReflectionToGrpcConfig } from 'nestjs-grpc-reflection';
import { join } from 'path';

export const grpcClientOptions = addReflectionToGrpcConfig({
  transport: Transport.GRPC,
  options: {
    url: 'localhost:5001',
    package: 'gtaxi',
    protoPath: join(__dirname, '../../proto/gtaxi.proto'),
  },
});
