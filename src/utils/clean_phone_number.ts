export const cleanPhoneNumber = (phoneNumber: string): string => {
  return phoneNumber
    .replaceAll(' ', '')
    .replaceAll('+', '')
    .replaceAll('(', '')
    .replaceAll(')', '')
    .replaceAll(',', '')
    .replaceAll('.', '')
    .trim();
};
