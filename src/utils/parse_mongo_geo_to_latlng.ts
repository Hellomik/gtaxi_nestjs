import { PointOrderDB } from 'src/modules/driver/model/db/point_order_db';
import { PointOrder } from 'src/proto/gtaxi';
import * as proto from 'src/proto/gtaxi';
import { MongoPoint } from './mongo_point';

export const parsePointOrderDbToPointOrder = (
  val: PointOrderDB,
): PointOrder => {
  return {
    point: {
      longitude: val.point.coordinates[0],
      latitude: val.point.coordinates[1],
    },
    type: val.type,
  };
};

export const parsePointOrderDbToLantLng = (
  val?: MongoPoint,
): proto.LatLng | undefined => {
  if (val == null) return undefined;
  return {
    latitude: val.coordinates[1],
    longitude: val.coordinates[0],
  };
};
