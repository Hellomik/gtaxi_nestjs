export interface MongoPoint {
  type: 'Point';
  //            long    lat
  coordinates: [number, number];
}
