import { Metadata } from '@grpc/grpc-js';
import { JwtService } from '@nestjs/jwt';
import { ObjectId } from 'mongodb';

export const getUserIdByMetadata = (
  metaData: Metadata,
  jwtService: JwtService,
) => {
  const header = metaData!.get('Authorization')[0].toString();
  const token: string = header.slice(header.indexOf(' ') + 1);
  const body = jwtService.verify(token);
  

  return new ObjectId(body.id);
};
