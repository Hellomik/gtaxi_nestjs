import {
  UseFilters,
  UseGuards,
  UsePipes,
  ValidationPipe,
  applyDecorators,
} from '@nestjs/common';
import { GrpcAuthGuard } from 'src/modules/user/services/grpc_auth_guard';
import { Http2gRPCExceptionFilter } from './http2gRPCException.filter';

export function ShieldDecorator() {
  return applyDecorators(
    UseGuards(GrpcAuthGuard),
    UsePipes(new ValidationPipe({ transform: true })),
    UseFilters(new Http2gRPCExceptionFilter()),
  );
}
