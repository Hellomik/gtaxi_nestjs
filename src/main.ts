import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { MicroserviceOptions, Transport } from '@nestjs/microservices';
import { join } from 'path';
import { addReflectionToGrpcConfig } from 'nestjs-grpc-reflection';
import { grpcClientOptions } from './utils/grpc_option';



async function bootstrap() {
  // const app = await NestFactory.create(AppModule);
  // await app.listen(3001);
  // console.log(grpcClientOptions);
  const appGrpc = await NestFactory.createMicroservice<MicroserviceOptions>(
    AppModule,
    grpcClientOptions,
  );

  appGrpc.listen();
}
bootstrap();
